<?php

declare(strict_types=1);

namespace App\Calculations;

use App\Repository\TransactionRepository;

class TransactionCalculator
{
    const DEFAULT_FEE = 10;
    const VIP_FEE = 5;
    const DAILY_TRANSACTIONS_AMOUNT_LIMIT_FOR_FEE = 100;

    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Calculate fee of transaction
     *
     * @param array - $data
     * @return float - fee
     */
    public function calculateFee(int $userId, float $amount): float
    {
        $result = $this->transactionRepository->getDailyTransactions($userId);
        $dailyAmount = (float)$result['daily_amount'];
        if ($dailyAmount > self::DAILY_TRANSACTIONS_AMOUNT_LIMIT_FOR_FEE) {
            $feePercentage = self::VIP_FEE / 100;
        } else {
            $feePercentage = self::DEFAULT_FEE / 100;
        }
        $fee = $amount * $feePercentage;
        return $fee;
    }
}
