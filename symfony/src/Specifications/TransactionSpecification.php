<?php

declare(strict_types=1);

namespace App\Specifications;

use App\Specifications\Specificators\TotalTransactionsAmountSpecification;
use App\Specifications\Specificators\TotalTransactionsCountSpecification;
use App\Specifications\Specificators\TransactionStatusSpecification;
use App\Constants\Constants;
use App\Repository\TransactionRepository;
use App\Entity\Transaction;
use App\Specifications\Specification;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TransactionSpecification implements Specification
{
    const TOTAL_TRANSACTIONS_AMOUNT_LIMIT_BY_CURRENCY = 1000;
    const TRANSACTIONS_COUNT_PER_HOUR = 10;

    private $data;
    private $transactionRepository;
    private $validator;

    public function __construct(TransactionRepository $transactionRepository, ValidatorInterface $validator)
    {
        $this->data = [];
        $this->transactionRepository = $transactionRepository;
        $this->validator = $validator;
    }
    /**
     * Checking requirements for transactions creation.
     * max 10 transactions per hour
     *
     * @param int $userId
     * @return void
     */
    public function passRequirements(int $userId, string $currency, float $fee, float $amount): void
    {
        $status = true;
        $message = '';
        $total = $amount + $fee;
        $totalTransactionsAmountSpecification = new TotalTransactionsAmountSpecification(self::TOTAL_TRANSACTIONS_AMOUNT_LIMIT_BY_CURRENCY);
        $satisfied = $totalTransactionsAmountSpecification->isSatisfiedBy(['total_amount' => $total]);
        if (!$satisfied) {
            $status = false;
            $message = 'Total transactions amount greather than ' . self::TOTAL_TRANSACTIONS_AMOUNT_LIMIT_BY_CURRENCY;
        }
        $totalTransactions = $this->transactionRepository->getCompletedTotalTransactions($userId, $currency);
        $totalTransactions['total_amount'] += $total;

        $totalTransactionsAmountSpecification = new TotalTransactionsAmountSpecification(self::TOTAL_TRANSACTIONS_AMOUNT_LIMIT_BY_CURRENCY);
        $satisfied = $totalTransactionsAmountSpecification->isSatisfiedBy($totalTransactions);
        if (!$satisfied) {
            $status = false;
            $message = 'Total transactions amount greather than ' . self::TOTAL_TRANSACTIONS_AMOUNT_LIMIT_BY_CURRENCY;
        }
        $date = date('Y-m-d H:i:s', strtotime('-1 hour'));
        $transactions = $this->transactionRepository->getByDate($userId, $date);
        $totalTransactionsCountSpecification = new TotalTransactionsCountSpecification(self::TRANSACTIONS_COUNT_PER_HOUR);
        $satisfied = $totalTransactionsCountSpecification->isSatisfiedBy($transactions);
        if (!$satisfied) {
            $status = false;
            $message = 'Maximum transactions count per hour reached';
        }
        $pendingTransaction = $this->transactionRepository->findBy(['user_id' => $userId, 'status' => Constants::STATUS_PENDING]);
        $statusSpecification = new TransactionStatusSpecification();
        $satisfied = $statusSpecification->isSatisfiedBy($pendingTransaction);
        if (!$satisfied) {
            $status = false;
            $message = 'Pending transactions not submited';
        }
        $this->data = ['success' => $status, 'message' => $message];
    }

    /**
     * Check if requirements satisfied
     *
     * @param ?array $data
     * @return bool true/false
     */
    public function isSatisfiedBy(?array $data): bool
    {
        if ($data['success']) {
            return true;
        }

        return false;
    }

    /**
     * Return data from requirements passing
     * @return array - $data
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Validate transaction object before proceed
     * @param object $data
     * @return array response
     */
    public function validate(object $data): array
    {
        $status = true;
        $message = '';
        $transaction = new Transaction();
        $transaction->setUserId($data->user_id);
        $transaction->setDetails($data->details);
        $transaction->setReceiverAccount($data->receiver_account);
        $transaction->setReceiverName($data->receiver_name);
        $transaction->setAmount($data->amount);
        $transaction->setFee(0);
        $transaction->setCurrency($data->currency);
        $transaction->setCreatedAt(new \DateTime());
        $transaction->setStatus('pending');
        $messages = $this->validator->validate($transaction);
        if (count($messages) > 0) {
            $status = false;
            $message = 'Passed data is not valid.';
        }
        return ['success' => $status, 'message' => $message, 'transaction' => $transaction];
    }
}
