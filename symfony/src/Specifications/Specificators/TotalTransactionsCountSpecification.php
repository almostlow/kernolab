<?php

declare(strict_types=1);

namespace App\Specifications\Specificators;

use App\Specifications\Specification;

class TotalTransactionsCountSpecification implements Specification
{
    private $maxCount;

    public function __construct(int $maxCount)
    {
        $this->maxCount = $maxCount;
    }

    public function isSatisfiedBy(?array $data): bool
    {
        if (count($data) >= $this->maxCount) {
            return false;
        }

        return true;
    }
}
