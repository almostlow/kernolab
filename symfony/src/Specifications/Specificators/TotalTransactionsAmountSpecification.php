<?php

declare(strict_types=1);

namespace App\Specifications\Specificators;

use App\Specifications\Specification;

class TotalTransactionsAmountSpecification implements Specification
{
    private $maxAmount;

    public function __construct(int $maxAmount)
    {
        $this->maxAmount = $maxAmount;
    }

    public function isSatisfiedBy(?array $data): bool
    {
        if (isset($data['total_amount']) && (float)$data['total_amount'] > $this->maxAmount) {
            return false;
        }

        return true;
    }
}
