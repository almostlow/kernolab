<?php

declare(strict_types=1);

namespace App\Specifications\Specificators;

use App\Specifications\Specification;

class TransactionStatusSpecification implements Specification
{
    public function isSatisfiedBy(?array $data): bool
    {
        if (isset($data) && !empty($data)) {
            return false;
        }

        return true;
    }
}
