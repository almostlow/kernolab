<?php

declare(strict_types=1);

namespace App\Specifications;

interface Specification
{
    public function isSatisfiedBy(?array $data): bool;
}
