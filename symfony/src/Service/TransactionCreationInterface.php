<?php

declare(strict_types=1);

namespace App\Service;

interface TransactionCreationInterface
{
    public function create(string $jsonContent): array;
}
