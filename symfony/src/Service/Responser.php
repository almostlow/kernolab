<?php

declare(strict_types=1);

namespace App\Service;

final class Responser
{
    public static function get(bool $status, array $data, string $message): array
    {
        return [
          'success' => $status,
          'data' => $data,
          'message' => $message,
        ];
    }
}
