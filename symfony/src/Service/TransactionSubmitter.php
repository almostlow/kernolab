<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\Responser;
use App\Rabbit\Sender;
use App\Constants\Constants;

class TransactionSubmitter
{
    const PIN = 111;

    private $transactionRepository;
    private $entityManager;
    private $sender;

    public function __construct(TransactionRepository $transactionRepository, EntityManagerInterface $entityManager, Sender $sender)
    {
        $this->transactionRepository = $transactionRepository;
        $this->entityManager = $entityManager;
        $this->sender = $sender;
    }

    /**
     * Checking requirements for transactions 2FA. If passed, change transaction status to submitted
     *
     * @param string $jsonContent
     */
    public function submit(string $jsonContent): array
    {
        $data = json_decode($jsonContent);
        $pin = $data->pin;
        $status = false;
        $message = 'Wrong pin';
        if ($pin === SELF::PIN) {
            $transaction =
                $this->transactionRepository->findOneBy([
                    'user_id' => $data->user_id,
                    'status' => Constants::STATUS_PENDING,
                ]);
            if (!empty($transaction)) {
                $transaction->setStatus(Constants::STATUS_SUBMITED);
                $this->entityManager->flush();
                $properties = $transaction->parseProperties();
                $this->sender->send(json_encode($properties));
                $status = true;
                $message = '';
            } else {
                $status = false;
                $message = 'No pending transactions';
            }
        }
        return Responser::get($status, [], $message);
    }
}
