<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\TransactionRepository;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\Responser;
use App\Constants\Constants;
use App\Service\TransactionCreationInterface;
use App\Specifications\TransactionSpecification;
use App\Calculations\TransactionCalculator;

class TransactionCreator implements TransactionCreationInterface
{
    private $transactionSpecification;
    private $entityManager;
    private $transactionCalculator;

    public function __construct(
        TransactionSpecification $transactionSpecification,
        EntityManagerInterface $entityManager,
        transactionCalculator $transactionCalculator
    ) {
        $this->transactionSpecification = $transactionSpecification;
        $this->transactionCalculator = $transactionCalculator;
        $this->entityManager = $entityManager;
    }

    /**
     * Checking requirements for transactions creation. If passed, create transaction in database.
     *
     * @param string $jsonContent
     * @return array response
     */
    public function create(string $jsonContent): array
    {
        $data = json_decode($jsonContent);
        $transaction = $this->transactionSpecification->validate($data);
        if (!$transaction['success']) {
            return Responser::get($transaction['success'], [], $transaction['message']);
        }
        $fee = $this->transactionCalculator->calculateFee($data->user_id, $data->amount);
        $this->transactionSpecification->passRequirements($data->user_id, $data->currency, $fee, $data->amount);
        $transactionData = $this->transactionSpecification->getData();
        if ($this->transactionSpecification->isSatisfiedBy($transactionData)) {
            $result = $this->insert($transaction['transaction'], $fee);
            if (isset($result['transaction']) && !empty($result['transaction'])) {
                $response = $result['transaction']->parseProperties();
            } else {
                $response = [];
            }
            return Responser::get($result['success'], ['transaction' => $response], $result['message']);
        } else {
            return Responser::get($transactionData['success'], [], $transactionData['message']);
        }
    }

    /**
     * Process transaction to database
     *
     * @param array - $data
     * @return array - result
     */
    private function insert(Transaction $transaction, float $fee): array
    {
        try {
            $transaction->setFee($fee);
            $this->entityManager->persist($transaction);
            $this->entityManager->flush();
            return ['success' => true, 'message' => '', 'transaction' => $transaction];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'Unexpected error'];
        }
    }
}
