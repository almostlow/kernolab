<?php

declare(strict_types=1);

namespace App\Service\Providers;

use App\Service\Providers\ProvidersInterface;

class MegaCash implements ProvidersInterface
{
    private $currencies;

    public function __construct()
    {
        $this->currencies = ['eur'];
    }

    /**
     * Get provider supported currencies
     * @return array - currencies
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * Process withdrawal to thirt part api
     * @param string $transactionData
     */
    public function withdraw(string $transactionData): bool
    {
        $providerData = substr($transactionData, 0, 20);
        return true;
    }
}
