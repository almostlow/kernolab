<?php

declare(strict_types=1);

namespace App\Service\Providers;

interface ProvidersInterface
{
    /**
     * Get provider supported currencies
     * @return array - currencies
     */
    public function getCurrencies(): array;

    /**
     * Process withdrawal to thirt part api
     * @param string $transactionData
     */
    public function withdraw(string $transactionData): bool;
}
