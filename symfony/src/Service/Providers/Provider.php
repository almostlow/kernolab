<?php

declare(strict_types=1);

namespace App\Service\Providers;

use App\Service\Providers\MegaCash;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\Providers\SuperMoney;
use App\Constants\Constants;

class Provider
{
    private $providers;
    private $entityManager;
    private $transactionRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        TransactionRepository $transactionRepository,
        MegaCash $megaCash,
        SuperMoney $superMoney
    ) {
        $this->providers = [$megaCash, $superMoney];
        $this->entityManager = $entityManager;
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Process withdrawal to provider from rabbit queue
     * @param array $data - transaction information
     * @return void
     */
    public function processWithdrawal(array $data)
    {
        $currency = $data['currency'];
        $transactionId = $data['id'];
        foreach ($this->providers as $provider) {
            if (empty($provider->getCurrencies()) || in_array($currency, $provider->getCurrencies())) {
                $currentProvider = $provider;
                break;
            }
        }
        if (!isset($currentProvider)) {
            return true;
        }
        $result = $currentProvider->withdraw(json_encode($data));
        $this->entityManager->clear();
        $transaction = $this->transactionRepository->find($transactionId);
        if ($result) {
            $transaction->setStatus(Constants::STATUS_COMPLETED);
        } else {
            $transaction->setStatus(Constants::STATUS_FAILED);
        }
        $this->entityManager->flush();
    }
}
