<?php

declare(strict_types=1);

namespace App\Service\Providers;

class SuperMoney implements ProvidersInterface
{
    private $currencies;

    public function __construct()
    {
        $this->currencies = [];
    }

    /**
     * Get provider supported currencies
     * @return array - currencies
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * Process withdrawal to thirt part api
     * @param string $transactionData
     */
    public function withdraw(string $transactionData): bool
    {
        $providerData = $transactionData . rand();
        return true;
    }
}
