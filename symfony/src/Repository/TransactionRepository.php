<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Constants\Constants;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    /**
     * Getting transactions count by date
     *
     * @param int $userId
     * @param string $date
     * @return integer - transactions count
     */
    public function getByDate(int $userId, string $date): array
    {
        $now = date('Y-m-d H:i:s');
        return $this->createQueryBuilder('t')
            ->andWhere('t.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('t.created_at >= :created_at')
            ->setParameter('created_at', $date)
            ->andWhere('t.created_at < :now')
            ->setParameter(':now', $now)
            ->getQuery()
            ->getResult();
    }

    /**
     * Getting daily transactions amount by user
     *
     * @param int $userId
     * @return array - daily transactions
     */
    public function getDailyTransactions(int $userId): ?array
    {
        $from = date('Y-m-d 00:00:00');
        $to = date('Y-m-d H:i:s');
        return $this->createQueryBuilder('t')
            ->andWhere('t.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('t.created_at >= :from')
            ->setParameter('from', $from)
            ->andWhere('t.created_at < :to')
            ->setParameter('to', $to)
            ->andWhere('t.status != :status')
            ->setParameter('status', Constants::STATUS_FAILED)
            ->select('SUM(t.amount) as daily_amount')
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Getting total transactions amount by user and currency
     * @return array - completed transactions
     */
    public function getCompletedTotalTransactions(int $userId, string $currency): ?array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->select('SUM(t.amount + t.fee) as total_amount')
            ->andWhere('t.currency = :currency')
            ->setParameter('currency', $currency)
            ->andWhere('t.status = :status')
            ->setParameter('status', Constants::STATUS_COMPLETED)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
