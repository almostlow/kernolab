<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Transaction;

class TransactionFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        //
        $transaction = new Transaction();
        $transaction->setUserId(1);
        $transaction->setDetails('test details');
        $transaction->setReceiverAccount('test receiver');
        $transaction->setReceiverName('test receiver name');
        $transaction->setAmount(22.22);
        $transaction->setFee(5);
        $transaction->setCurrency('eur');
        $transaction->setCreatedAt(new \DateTime());
        $transaction->setStatus('pending');
        $manager->persist($transaction);
        $manager->flush();
    }
}
