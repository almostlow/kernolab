<?php

declare(strict_types=1);

namespace App\Constants;

final class Constants
{
    const STATUS_PENDING = 'pending';
    const STATUS_SUBMITED = 'submited';
    const STATUS_COMPLETED = 'completed';
    const STATUS_FAILED = 'failed';
}
