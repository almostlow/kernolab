<?php

declare(strict_types=1);

namespace App\Rabbit;

use Psr\Container\ContainerInterface;

class Sender
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function send(string $jsonContent): void
    {
        $queueExist = $this->container->has('old_sound_rabbit_mq.transactions_processing_producer');
        if ($queueExist) {
            $queue = $this->container->get('old_sound_rabbit_mq.transactions_processing_producer');
            $queue->setContentType('application/json');
            $queue->publish($jsonContent);
        }
    }
}
