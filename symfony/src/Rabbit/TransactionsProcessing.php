<?php

declare(strict_types=1);

namespace App\Rabbit;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use App\Service\Providers\Provider;

class TransactionsProcessing implements ConsumerInterface
{
    private $provider;

    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    public function execute(AMQPMessage $msg): void
    {
        $body = $msg->body;
        $response = json_decode($msg->body, true);
        $this->provider->processWithdrawal($response);
    }
}
