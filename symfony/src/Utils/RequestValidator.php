<?php

declare(strict_types=1);

namespace App\Utils;

final class RequestValidator
{

    /**
     * Validate if all request fields is present in request and with valid formats
     * @param $content - stringified json
     */
    public static function validateRequest(string $content, array $fields): array
    {
        $data = self::validateJson($content);
        if (!empty($data['error'])) {
            return ['status' => false, 'message' => $data['error']];
        }
        foreach ($fields as $field) {
            if (!isset($data['result']->{$field})) {
                return ['status' => false, 'message' => $field . ' not specified'];
            }
        }
        foreach ($data['result'] as $field => $value) {
            if (!in_array($field, $fields)) {
                return ['status' => false, 'message' => 'Unknown field ' . $field];
            }
        }
        return ['status' => true, 'message' => ''];
    }

    /**
     * Prevent from send bad content in request
     * @param $content - stringify json
     */

    public static function validateJson(string $content): array
    {
        $result = json_decode($content);
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = '';
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }
        return ['result' => $result, 'error' => $error];
    }
}
