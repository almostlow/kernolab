<?php

namespace App\Utils;

final class Utils
{
    /**
     * Convert snake_case to camelCase
     *
     * @param string $replacer
     * @param string $string
     * @return string $string
     */
    public static function toCamelCase(string $replacer, string $string): string
    {
        $str = str_replace($replacer, '', ucwords($string, $replacer));
        $str = ucfirst($str);
        return $str;
    }
}
