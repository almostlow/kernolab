<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\TransactionCreator;
use App\Service\TransactionSubmitter;
use App\Service\Responser;
use App\Utils\RequestValidator;
use App\Repository\TransactionRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiController extends AbstractController
{
    /**
     * Example request
     * {
     *   "user_id": 1,
     *   "details": "Transaction number one",
     *   "receiver_account": "12345",
     *   "receiver_name": "Name Surname",
     *   "amount": 20.00,
     *   "currency": "eur"
     * }
     *
     * Example response
     * {
     *   "success": true,
     *   "data": {//some data},
     *   "message": "some message"
     * }
     * @param Request $request
     * @param TransactionCreator $transactionCreator
     * @param LoggerInterface $logger
     *
     * @Route("/api/withdrawal_request", methods={"POST"})
     */
    public function withdrawalRequest(Request $request, TransactionCreator $transactionProcessor, LoggerInterface $logger): JsonResponse
    {
        try {
            if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                $requiredFields = ['user_id', 'details', 'receiver_account', 'receiver_name', 'amount', 'currency'];
                $valid = RequestValidator::validateRequest($request->getContent(), $requiredFields);
                if (!$valid['status']) {
                    return new JsonResponse(Responser::get($valid['status'], [], $valid['message']));
                }
                $result = $transactionProcessor->create($request->getContent());
                $status = $result['success'] ? 201 : 200;
                return new JsonResponse($result, $status);
            } else {
                return new JsonResponse(Responser::get(false, [], 'Wrong request format'));
            }
        } catch (\Exception $e) {
            $logger->critical('withdrawalRequest ' . $e->getMessage());
            return new JsonResponse(Responser::get(false, [], 'Unexpected error'), 500);
        }
    }

    /**
     * Example request
     * {
     *   "pin": 111,
     *   "user_id": 1
     * }
     *
     * Example response
     * {
     *   "success": true,
     *   "data": {//some data},
     *   "message": "some message"
     * }
     * @param Request $request
     * @param TransactionSubmitter $transactionSubmitter
     * @param LoggerInterface $logger
     *
     * @Route("/api/submit_transaction", methods={"POST"})
    */
    public function submitTransaction(Request $request, TransactionSubmitter $transactionSubmitter, LoggerInterface $logger): JsonResponse
    {
        try {
            if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                $requiredFields = ['user_id', 'pin'];
                $valid = RequestValidator::validateRequest($request->getContent(), $requiredFields);
                if (!$valid['status']) {
                    return new JsonResponse(Responser::get($valid['status'], [], $valid['message']));
                }
                $result = $transactionSubmitter->submit($request->getContent());
                return new JsonResponse($result);
            } else {
                return new JsonResponse(Responser::get(false, [], 'Wrong request format'));
            }
        } catch (\Exception $e) {
            $logger->critical('submitTransaction ' . $e->getMessage());
            return new JsonResponse(Responser::get(false, [], 'Unexpected error'), 500);
        }
    }

    /**
     * Example request
     * {
     *   "transaction_id": 1
     * }
     *
     * Example response
     * {
     *   "success": true,
     *   "data": {//some data},
     *   "message": "some message"
     * }
     * @param Request $request
     * @param TransactionRepository $transactionRepository
     * @param LoggerInterface $logger
     *
     * @Route("/api/get_transaction", methods={"POST"})
     */
    public function getTransaction(Request $request, TransactionRepository $transactionRepository, LoggerInterface $logger): JsonResponse
    {
        try {
            if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                $requiredFields = ['transaction_id'];
                $valid = RequestValidator::validateRequest($request->getContent(), $requiredFields);
                if (!$valid['status']) {
                    return new JsonResponse(Responser::get($valid['status'], [], $valid['message']));
                }
                $data = json_decode($request->getContent());
                $transaction = $transactionRepository->find((int)$data->transaction_id);
                if (!empty($transaction)) {
                    $formatedTransaction = $transaction->parseProperties();
                    return new JsonResponse(Responser::get(true, ['transaction' => $formatedTransaction], ''));
                }
                return new JsonResponse(Responser::get(false, [], 'Transaction not found'), 404);
            } else {
                return new JsonResponse(Responser::get(false, [], 'Wrong request format'));
            }
        } catch (\Exception $e) {
            $logger->critical('getTransaction ' . $e->getMessage());
            return new JsonResponse(Responser::get(false, [], 'Unexpected error'), 500);
        }
    }
}
