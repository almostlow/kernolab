<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Utils\Utils;

class UtilsTest extends TestCase
{
    public function testToCamelCase(): void
    {
        $res = Utils::toCamelCase('_', 'test_word');
        $this->assertEquals('TestWord', $res);
    }
}