<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Service\TransactionSubmitter;
use App\Utils\RequestValidator;

class RequestValidatorTest extends TestCase
{
    public function testValidateRequestSuccess(): void
    {
        $res = RequestValidator::validateRequest('{
			"user_id": 1,
			"details": "Transaction number one",
			"receiver_account": "12345",
			"receiver_name": "Name Surname",
			"amount": 2000.00,
			"currency": "eur"
		}', ['user_id', 'details', 'receiver_account', 'receiver_name', 'amount', 'currency']);
		$this->assertEquals(['status' => true, 'message' => ''], $res);
    }

    public function testValidateRequestFailure(): void
    {
        $res = RequestValidator::validateRequest('{
			"user_id": 1,
			"details": "Transaction number one",
			"receiver_account": "12345",
			"receiver_name": "Name Surname",
			"amount": 2000.00,
			"currency": "eur"
		}', ['user_id', 'details', 'receiver_account', 'receiver_name', 'amount']);
		$this->assertEquals(['status' => false, 'message' => 'Unknown field currency'], $res);
    }
}