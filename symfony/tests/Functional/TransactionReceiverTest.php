<?php

// tests/Repository/ProductRepositoryTest.php
namespace App\Tests\Functional;

use App\Entity\Transaction;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TransactionRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearchByUserId()
    {
        $product = $this->entityManager
            ->getRepository(Transaction::class)
            ->findOneBy(['user_id' => 1])
        ;

        $this->assertSame(22.22, $product->getAmount());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}