<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    public function testCheckWithdrawalResponseMessage()
    {
        $client = static::createClient();
        $client->request('POST', '/api/withdrawal_request');
        $message = json_decode($client->getResponse()->getContent())->message;
        $this->assertEquals("Wrong request format", $message);
    }

    public function testCheckSubmitTransactionResponseMessage()
    {
        $client = static::createClient();
        $client->request('POST', '/api/submit_transaction');
        $message = json_decode($client->getResponse()->getContent())->message;
        $this->assertEquals("Wrong request format", $message);
    }

    public function testCheckGetTransactionResponseMessage()
    {
        $client = static::createClient();
        $client->request('POST', '/api/get_transactions');
        $message = json_decode($client->getResponse()->getContent())->message;
        $this->assertEquals("Wrong request format", $message);
    }
}