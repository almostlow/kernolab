# Setup
  
From root directory:  
  
``docker-compose build``  
then  
``docker-compose up``

Install all dependencies:  
``docker exec -it container_id composer install``  
  
Setup rabbitmq env:  
``docker exec -it container_id php bin/console rabbitmq:setup-fabric``  

Run background, that reading messages from queue:  
``docker exec -it container_id php bin/console rabbitmq:consumer transactions_processing``  

Also need add kernolab.local to your hosts file, with docker ip, for example:
``192.168.99.100 kernolab.local`` windows case.
Also hardcoded parameter left in ``services.yaml``, ``.env`` and ``.env.test``, change to your docker ip.  
  
## Access:
Api: http://kernolab.local  
RabbitMQ: http://kernolab.local:15672/#/queues/%2F/transactions_processing. kernolab is your docker host. (user: user, pw: bitnami)    
No frontend provided, use postman or similar program to make requests. Request/Response examples in ``ApiController.php``
  
# Notes
It was decided that transactions would be processed by the queue mechanism rather than the background process because no suitable symfony component was found, as well as the queue mechanism a better solution for transaction processing.
Tests written only for utils functions, other functions requiring database access or is very simple, so decided currently not do this.

I think, that all created transactions has to be submited in some interval of time by 2FA, if not, they are declined or deleted.  
So while transactions are pending, user cannot create another one. Old transactions deleting should be created by cron job. Currently not implemented.